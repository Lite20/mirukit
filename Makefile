CXX = g++
RM  = rm -f

# colors make everything better
CYAN = \033[0;36m
RED  = \033[0;31m
NC   = \033[0m

# determine architecture
ifeq ($(shell uname -m), x86_64)
ARCH = 64
else
ARCH = 32
endif

#determine PLATFORM dependant variable values
ifeq ($(shell uname -s), Linux)
PLATFORM = LINUX

#change `opencv` to `opencv4` to use opencv4
CXXFLAGS = `pkg-config --cflags opencv` -Wall -Wextra -pedantic -std=c++11 -m$(ARCH)
LDLIBS   = `pkg-config --libs opencv`

else

PLATFORM = WINDOWS
EXEC_EXTENSION = .exe
CXXFLAGS = -Iinclude -Wall -Wextra -pedantic -std=c++11 -m$(ARCH)
LDLIBS   = -Llib -lopencv_core411 -lopencv_calib3d411 -lopencv_features2d411 -lopencv_highgui411 -lopencv_ml411 -lopencv_stitching411 -lopencv_core411 -lopencv_flann411 -lopencv_imgcodecs411 -lopencv_objdetect411 -lopencv_video411 -lopencv_dnn411 -lopencv_gapi411 -lopencv_imgproc411 -lopencv_photo411 -lopencv_videoio411

endif


SRCDIR = src
BUILDDIR = build

EXEC = $(BUILDDIR)/mirukit$(EXEC_EXTENSION)
SRCS := $(shell find $(SRCDIR)/ -name *.cc)
OBJS := $(subst .cc,.o,$(SRCS))
OBJS := $(subst $(SRCDIR),$(BUILDDIR),$(OBJS))

all: $(EXEC)

release: clean .release

.release: CXXFLAGS += -O3
.release: $(EXEC)

$(EXEC): $(OBJS)
	$(CXX) -o $(EXEC) $(OBJS) $(LDLIBS)

# implicit rule to compile source files in SRCDIR to object files in the BUILDDIR
$(BUILDDIR)/%.o: $(SRCDIR)/%.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CXXFLAGS) -MM $^ | sed 's|[a-zA-Z0-9_-]*\.o|$(BUILDDIR)/&|' > ./.depend;

# call to check what your system is being detected as
about:
	@echo -e "$(CYAN)Compiling for $(ARCH) bit $(PLATFORM)$(NC)"

# call clean object files
clean:
	$(RM) $(shell find $(BUILDDIR)/ -name *.o)

# call to clean entirely & reset
reset: clean
	$(RM) *~ .depend
	$(RM) $(EXEC)

include .depend